require([
    "esri/Map",
    "esri/views/MapView",
    "esri/tasks/support/Query",
    "esri/tasks/QueryTask",
    "esri/Graphic",
    "esri/geometry/Point",
    "esri/geometry/geometryEngine"
],
function(
    Map, 
    MapView,
    Query,
    QueryTask,
    Graphic,
    Point,
    geometryEngine
) {

    var map = new Map({
        basemap: "dark-gray"
    });

    var view = new MapView({
        container: "viewMap",
        map: map,
        center: [117.704370, 2.178113],
        zoom: 6
    });
});
