var map;
var view;
var graphic;

require([
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/FeatureLayer",
    "esri/tasks/support/Query",
    "esri/tasks/QueryTask",
    "esri/Graphic"
],
function(
    Map, 
    MapView,
    FeatureLayer,
    Query,
    QueryTask,
    Graphic
) {
    map = new Map({
        basemap: "dark-gray"
    });

    view = new MapView({
        container: "viewMap",
        map: map,
        center: [117.704370, 2.178113],
        zoom: 6
    });

    graphic = Graphic

    var checkPointInfo = {
        title: "{NAME}",
        content: "{*}"
    }

    var checkPointRenderer = {
        "type": "simple",
        "symbol": {
            "type": "picture-marker",
            "url": "https://ik.imagekit.io/kzo4xymlu/map_tIUjGv9m4.png",
            "width": "14px",
            "height": "16px"
        }
    }

    var checkPointLayer = new FeatureLayer({
        url: "https://services5.arcgis.com/sZhIjUPk43mx2p8A/arcgis/rest/services/check_point/FeatureServer",
        renderer: checkPointRenderer,
        outFields: ["NAME", "TYPE", "PARAMETER"],
        popupTemplate: checkPointInfo,
        definitionExpression: "TYPE = 'Check Point'"
    });

    map.add(checkPointLayer);

    var portRenderer = {
        "type": "simple",
        "symbol": {
            "type": "picture-marker",
            "url": "https://ik.imagekit.io/kzo4xymlu/anchor_SqyVWiBCT.png",
            "width": "24px",
            "height": "24px"
        }
    }

    var portLayer = new FeatureLayer({
        url: "https://services5.arcgis.com/sZhIjUPk43mx2p8A/arcgis/rest/services/check_point/FeatureServer",
        renderer: portRenderer,
        outFields: ["NAME", "TYPE", "PARAMETER"],
        popupTemplate: checkPointInfo,
        definitionExpression: "TYPE = 'Port'"
    });

    map.add(portLayer);


    var bridgeRenderer = {
        "type": "simple",
        "symbol": {
            "type": "picture-marker",
            "url": "https://ik.imagekit.io/kzo4xymlu/bridge_GqeBu1nM2.png",
            "width": "24px",
            "height": "24px"
        }
    }

    var bridgeLayer = new FeatureLayer({
        url: "https://services5.arcgis.com/sZhIjUPk43mx2p8A/arcgis/rest/services/check_point/FeatureServer",
        renderer: bridgeRenderer,
        outFields: ["NAME", "TYPE", "PARAMETER"],
        popupTemplate: checkPointInfo,
        definitionExpression: "TYPE = 'Brigde'"
    });

    map.add(bridgeLayer);

    // Define query sql expression for get Bridge data
    var queryBrigde = new Query();
    queryBrigde.where = "TYPE = 'Brigde'"
    queryBrigde.outFields = ["*"];
    queryBrigde.returnGeometry = true;

    var queryBrigdeTask = new QueryTask({
        url: "https://services5.arcgis.com/sZhIjUPk43mx2p8A/arcgis/rest/services/check_point/FeatureServer/0" 
    });
    
    queryBrigdeTask.execute(queryBrigde)
        .then(function(result){
            var simpleFillSymbol = {
                type: "simple-fill",
                color: [227, 139, 79, 0.8],
                outline: {
                  color: [255, 255, 255],
                  width: 1
                }
            };
            
            result.features.forEach(function(item){
                var rings = [];
                var array = item.attributes.parameter.split(';');

                while (array.length) {
                    rings.push(array.splice(0, 2));
                }

                var polygon = {
                    type: "polygon",
                    rings: rings
                };

                var polygonGraphic = new Graphic({
                    geometry: polygon,
                    symbol: simpleFillSymbol
                });
        
                view.graphics.add(polygonGraphic);
            })
        })
});
