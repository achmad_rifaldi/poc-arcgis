const TUGBOAT_DATA = [
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "05-Mar-18 00:37:17",
    latitude: -0.898537,
    longitude: 117.643474,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "Sungai Meriam"
  },
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "05-Mar-18 04:37:15",
    latitude: -0.695310,
    longitude: 117.728619,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "Tg. Aju"
  },
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "05-Mar-18 05:37:14",
    latitude: -0.6225310,
    longitude: 117.753338,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "KTD"
  },
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "05-Mar-18 09:37:14",
    latitude: -0.405044,
    longitude: 117.704886,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "Sungai Lais"
  },
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "05-Mar-18 10:37:38",
    latitude: -0.348741,
    longitude: 117.684287,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "TCM"
  },
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "05-Mar-18 11:37:14",
    latitude: -0.325166,
    longitude: 117.622031,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "MBR Anchorage"
  },
  {
    company: "PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "06-Mar-18 05:37:13",
    latitude: -0.275957,
    longitude: 117.843589,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "Sungai Lais"
  },
  {
    company: " PT. Pancaran Samudera Transport",
    tug_name: "Pancaran 1312",
    barge_name: "PST 1312",
    route_name: "JBM-MBR",
    date: "06-Mar-18 02:37:40",
    latitude: -0.32338,
    longitude: 117.623310,
    status: "Loaded Transit",
    eta: "08/03/2018 11:37:14",
    proximity: "MBR Anchorage"
  }
]
